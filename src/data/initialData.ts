import  { RegisterModel } from '../types/Register';

export const REGISTER: RegisterModel = {
    firstName: '',
    lastName: '',
    email: '',
    mobNo: '',
    location: '',
    schoolProf: '',
    timeSlot: '',
    id: '',
    createdDate: '',
}