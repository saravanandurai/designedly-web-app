

export interface RegisterModel {
    firstName?: string,
    lastName?: string,
    email?: string,
    mobNo?: string,
    location?: string,
    schoolProf?: string,
    timeSlot?: string,
    id?:  string,
    createdDate?: string
}