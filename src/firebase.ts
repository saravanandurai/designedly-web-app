import * as firebase from 'firebase/app';
import 'firebase/firestore'
import { FIREBASE_CONFIG } from './config'


const firebaseApp = firebase.initializeApp(FIREBASE_CONFIG);

export const db = firebaseApp.firestore();