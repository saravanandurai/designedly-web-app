import Vue from 'vue';
import Vuex from 'vuex';

import RegisterModule from './register';

Vue.use(Vuex);

export default new Vuex.Store({
  state: {

  },
  mutations: {
   
  },
  actions: {

  },
  modules: {
    register: RegisterModule
  }
});
